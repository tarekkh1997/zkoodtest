import React, { useEffect, useState } from "react";
import Autocomplete from "./components/Autocomplete/Autocomplete";
import "./components/Autocomplete/styles.css";




const App = () => {
  const [result, setResult] = useState([]);
  const [loading, setLoading] = useState(false);


  useEffect(() => {
    const getTodos = async () => {
      setLoading(true)
      fetch('https://jsonplaceholder.typicode.com/todos')
        .then(response => response.json())
        .then(json => {
          console.log(json)
          setResult(json)
        })
      setLoading(false)

    };

    getTodos();
  }, []);


  return (
    <div className="root">
      <h1>React Autocomplete Demo</h1>
      <h2>Start typing and experience React autocomplete!</h2>
      {loading ? <div>loading.....</div> : <Autocomplete
        suggestions={result}
      />}
    </div>
  );
};

export default App;